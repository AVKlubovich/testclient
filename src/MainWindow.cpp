#include "Common.h"

#include "MainWindow.h"
#include "ui_MainWindow.h"

#include "network-core/RequestsManager/RequestsManager.h"
#include "network-core/RequestsManager/OnlyJsonRequest.h"
#include "network-core/RequestsManager/Response.h"

#include "utils/Settings/Settings.h"
#include "utils/Settings/SettingsFactory.h"


using namespace test_client;

MainWindow::MainWindow()
    : QMainWindow(nullptr)
    , _ui(new Ui::MainWindow)
    , _requestsManager(network::RequestsManagerShp::create())
{
    _ui->setupUi(this);

    setWindowTitle(WINDOW_TITLE);

    connect(_ui->btnOpen, &QPushButton::clicked, this, &MainWindow::onBtnOpenClicked);
    connect(_ui->btnSave, &QPushButton::clicked, this, &MainWindow::onBtnSaveClicked);
    connect(_ui->btnSaveAs, &QPushButton::clicked, this, &MainWindow::onBtnSaveAsClicked);
    connect(_ui->btnSend, &QPushButton::clicked, this, &MainWindow::onBtnSendClicked);

    connect(_ui->btnSettings, &QPushButton::clicked, this, &MainWindow::onBtnSettingsClicked);
    connect(_ui->btnBack, &QPushButton::clicked, this, &MainWindow::onBtnBackClicked);
    connect(_ui->comboBSettings, &QComboBox::currentTextChanged, this, &MainWindow::onSettingsChanged);
}

MainWindow::~MainWindow()
{
    delete _ui;
}

void MainWindow::onBtnOpenClicked()
{
    QDir openDir(QDir::current());
    openDir.cd("../TestClient/test_requests");
    const QString& fileName = QFileDialog::getOpenFileName(this,
                                                           tr("Open JSON"),
                                                           openDir.absolutePath(),
                                                           tr("JSON Files (*.json)"));

    if (fileName.isEmpty())
    {
        return;
    }

    QFile file(fileName);
    if (!file.open(QFile::ReadOnly))
    {
        _ui->statusBar->showMessage(QString("Unable to open to read file %1").arg(fileName), 3);
        return;
    }

    setCurrentFileName(fileName);

    _ui->editOut->setText(file.readAll());
    _ui->editIn->clear();

    file.close();

    _ui->btnSave->setEnabled(true);
    _ui->btnSaveAs->setEnabled(true);
}

void MainWindow::onBtnSaveClicked()
{
    if (_currentOpenDoc.isEmpty())
        return;

    QFile newFile(_currentOpenDoc);
    if (!newFile.open(QFile::WriteOnly))
    {
        _ui->statusBar->showMessage(QString("Unable to open to write file %1").arg(_currentOpenDoc), 3);
        return;
    }

    const auto& newData = _ui->editOut->toPlainText();
    newFile.write(newData.toUtf8());
    newFile.close();
}

void MainWindow::onBtnSaveAsClicked()
{
    const auto& fileName = QFileDialog::getSaveFileName(this,
                                                        "Save JSON",
                                                        _currentOpenDoc,
                                                        "JSON Files (*.json)");

    if (fileName.isEmpty())
        return;

    QFile newFile(fileName);
    if (!newFile.open(QFile::WriteOnly))
    {
        _ui->statusBar->showMessage(QString("Unable to open to write file %1").arg(_currentOpenDoc), 3);
        return;
    }

    const auto& newData = _ui->editOut->toPlainText();
    newFile.write(newData.toUtf8());
    newFile.close();

    setCurrentFileName(fileName);
}

void MainWindow::onBtnSendClicked()
{
//    _ui->btnSend->setEnabled(false);
//    _ui->btnOpen->setEnabled(false);

    _ui->editIn->clear();

    const auto& requestStr = _ui->editOut->toPlainText();
    const auto& request = network::OnlyJsonRequestShp::create(requestStr);
    _requestsManager->execute(request, this, &MainWindow::onResponse);

    _timer.start();
}

void MainWindow::onResponse(network::ResponseShp response)
{
//    _ui->btnSend->setEnabled(true);
//    _ui->btnOpen->setEnabled(true);

    if (!response)
    {
        qDebug() << __FUNCTION__ << "Response is not valid";
        _ui->statusBar->showMessage("Response is not valid", 3);
        return;
    }

    _ui->statusBar->showMessage(QString("Response timeout: %1").arg(_timer.elapsed()), 2500);

    QString str;
    const auto& data = response->toVariant();
    const auto& list = QJsonDocument::fromVariant(data).toJson().split('\n');
    for (const auto& string : list)
    {
        str.append(qPrintable(string));
        str.append("\n");
    }

    _ui->editIn->setText(str);
}

void MainWindow::onBtnSettingsClicked()
{
    _ui->stackedWidget->setCurrentWidget(_ui->settingsPage);

    if (_ui->comboBSettings->count() != 0)
        return;

    _ui->comboBSettings->blockSignals(true);
    const QStringList& settingsList = utils::SettingsFactory::instance().availableSettings();
    _ui->comboBSettings->addItems(settingsList);

    const QString& currentSetting = utils::SettingsFactory::instance().currentSettingsName();
    _ui->comboBSettings->setCurrentText(currentSetting);
    _ui->comboBSettings->blockSignals(false);
}

void MainWindow::onBtnBackClicked()
{
    _ui->stackedWidget->setCurrentWidget(_ui->workPage);
}

void MainWindow::onSettingsChanged(const QString& currentSetting)
{
    if (utils::SettingsFactory::instance().setCurrentSettings(currentSetting))
    {
        const QString& msg = QString("current setting is %1").arg(currentSetting);
        qDebug() << msg;
        _ui->statusBar->showMessage(msg, 3);
    }
}

void MainWindow::setCurrentFileName(const QString& fileName)
{
    _currentOpenDoc = fileName;

    setWindowTitle(QString("%1 %2").arg(WINDOW_TITLE).arg(fileName));
}
