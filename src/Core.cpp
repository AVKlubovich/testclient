﻿#include "Common.h"
#include "Core.h"

#include "utils/Settings/Settings.h"
#include "utils/Settings/SettingsFactory.h"

#include "utils/Logging/Logger.h"
#include "utils/Logging/LoggerMessages.h"
#include "utils/Logging/Devices/FileDevice.h"
#include "utils/Logging/Devices/DebuggerDevice.h"
#include "utils/Logging/Devices/ConsoleDevice.h"

#include "Definitions.h"

#include "MainWindow.h"


using namespace test_client;

Core::Core()
{
    QDir::setCurrent( QCoreApplication::applicationDirPath() );
}

bool Core::init()
{
    qsrand(QTime(0, 0, 0).msecsTo(QTime::currentTime()));

    if (!readConfig())
    {
        qDebug() << "Cannot read config";
        return false;
    }

    if (!initLogger())
    {
        qDebug() << "Logger does not initialize";
        return false;
    }

    if (!initWindow())
    {
        qWarning() << "Could not initialize server";
        return false;
    }

    return true;
}

void Core::run()
{
    _window->show();
}

void Core::done()
{
    _window.clear();
    _logger.clear();
}

bool Core::readConfig()
{
    QDir settingsDir(CONFIG_DIR);
    if (!settingsDir.exists())
    {
        if (!settingsDir.mkdir(QString("../%1").arg(CONFIG_DIR)))
        {
            return false;
        }
    }

    QStringList filters;
    filters << "*.ini" << "*.in";
    const QFileInfoList& files = settingsDir.entryInfoList(filters, QDir::Readable | QDir::Files);
    if (files.isEmpty())
    {
        utils::Settings::Options config = { "configuration/client.ini", true };
        utils::SettingsFactory::instance().registerSettings("client", config);

        auto settings = utils::SettingsFactory::instance().settings("client");
        settings =
        {
            // Connection
            { "Connection/Scheme", "http" },
            { "Connection/Host", "localhost" },
            { "Connection/Port", "81" },

            // Logs
            { "Log/EnableLog", true },
            { "Log/FlushInterval", 1000 },
            { "Log/PrefixName", "test_client.log" },
            { "Log/Dir", "./logs/" },
            { "Log/MaxSize", 134217728 }, // 100 mb
        };

        utils::SettingsFactory::instance().setCurrentSettings("client");

        return true;
    }

    for (const QFileInfo& file : files)
    {
        utils::Settings::Options config = { file.absoluteFilePath(), true };
        utils::SettingsFactory::instance().registerSettings(file.baseName(), config);
    }

    utils::SettingsFactory::instance().setCurrentSettings(files.first().baseName());

    return true;
}

bool Core::initLogger()
{
    // TODO: http://134.17.26.128:8080/browse/OKK-125
//    return true;

    auto settings = utils::SettingsFactory::instance().currentSettings();
    settings.beginGroup("Log");

    if (!settings["Enable"].toBool())
    {
        qDebug() << "logger disabled";
        return true;
    }

    _logger = QSharedPointer<utils::Logger>::create();

    const auto loggerOptions = QSharedPointer<utils::LoggerMessages::Options>::create();
    loggerOptions->timerInterval = settings["FlushInterval"].toInt();
    if (!_logger->init(loggerOptions))
        return false;

    // FileDevice
    const auto fileOptions = QSharedPointer<utils::FileDevice::FileOptions>::create();
    fileOptions->maxSize = settings["MaxSize"].toLongLong();
    fileOptions->prefixName = settings["PrefixName"].toString(); //"okk_server.log";
    fileOptions->directory = settings["Dir"].toString();

    if (!_logger->addDevice(fileOptions))
        return false;

    // DebuggerDevice
    const auto debuggerDevice = QSharedPointer<utils::DebuggerDevice::DebuggerOptions>::create();

    if (!_logger->addDevice(debuggerDevice))
        return false;

    // ConsoleDevice
    const auto consoleDevice = QSharedPointer<utils::ConsoleDevice::ConsoleOptions>::create();

    if (!_logger->addDevice(consoleDevice))
        return false;

    qDebug() << "initLogger";
    return true;
}

bool Core::initWindow()
{
    _window = test_client::MainWindowShp::create();

    return true;
}
