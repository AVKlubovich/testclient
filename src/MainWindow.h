﻿#pragma once


namespace network
{
    class Response;
    typedef QSharedPointer<Response> ResponseShp;

    class RequestsManager;
    typedef QSharedPointer<RequestsManager> RequestsManagerShp;
}

namespace Ui
{
    class MainWindow;
}

namespace test_client
{

    class MainWindow : public QMainWindow
    {
        Q_OBJECT

    public:
        explicit MainWindow();
        ~MainWindow();

    private slots:
        void onBtnOpenClicked();
        void onBtnSaveClicked();
        void onBtnSaveAsClicked();

        void onBtnSendClicked();
        void onResponse(network::ResponseShp response);

        void onBtnSettingsClicked();
        void onBtnBackClicked();

        void onSettingsChanged(const QString& currentSetting);

    private:
        void setCurrentFileName(const QString& fileName);

    private:
        Ui::MainWindow *_ui;

        network::RequestsManagerShp _requestsManager;

        QString _currentOpenDoc;

        const QString WINDOW_TITLE = "Test your luck.";

        QElapsedTimer _timer;
    };

}
