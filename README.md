
# test client

## Описание проекта
Тестовый клиент для проверки серверов

## Используемые технологии
[C++14](https://ru.wikipedia.org/wiki/C%2B%2B14)

## Системные требования
Windows  
Linux

## Документация
[Соглашения по оформлению кода](https://drive.google.com/open?id=0B48GpktEZIksWjNjTmdWcW53Rnc)

## Список контрибьюторов
[@github/avklubovich](../../../../avklubovich)
